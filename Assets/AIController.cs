﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AIController : BaseController
{
    public enum AI_STATE
    {
        Idle,
        Cooking,
        Attack,
        Busy,
        StandBy,
        UseSupportCard,
        BuySpecialCard,
    }

    public AI_STATE MyState = AI_STATE.Idle;

    protected override void Start()
    {
        Name = $"AI:{gameObject.GetInstanceID()}";
        base.Start();
    }

    private void Update()
    {
        if (amILock) return;

        switch(MyState)
        {
            case AI_STATE.Idle:
                MyState = AI_STATE.BuySpecialCard;
                break;
            case AI_STATE.UseSupportCard:
                MyState = AI_STATE.Busy;
                StartCoroutine(UseSupportCard());
                break;
            case AI_STATE.Cooking:
                MyState = AI_STATE.Busy;
                StartCoroutine(MakeRecipe());
                break;
            case AI_STATE.Attack:
                MyState = AI_STATE.Busy;
                StartCoroutine(AttackOther());
                break;
            case AI_STATE.BuySpecialCard:
                MyState = AI_STATE.Busy;
                BuySpecialCard();
                break;
            default:
                break;
        }
        
    }

    public override void BuySpecialCard()
    {
        int roll = Random.Range(1, 100);
        
        if (Coin >= 2 && roll <= 60) base.BuySpecialCard();

        MyState = AI_STATE.Attack;
    }

    protected virtual IEnumerator AttackOther()
    {
        var query = specialCard.Where(i => i.GetComponent<SpecialCard>().SpecialCardType == SpecialCardType.Attack).ToList();

        if (query.Count() < 1)
        {
            MyState = AI_STATE.UseSupportCard;
            yield break;
        }

        foreach (var item in query)
        {
            // 有 50% 的機率不使用攻擊牌
            int randomN = Random.Range(1, 100);
            if (randomN <= 50) continue;

            // 隨機目標
            var RandomTarget = GameManager.Instance.players
                .Where(player => player != this)
                .OrderBy(x => Random.value)
                .First();

            // 最有錢目標
            var RichTarget = GameManager.Instance.players.
                Where(player => player != this).
                OrderByDescending(player => player.Coin).First();

            // 如果最有錢目標的$ = 0，則隨機挑選目標
            if (RichTarget.Coin == 0) RichTarget = RandomTarget;

            // 食材最多目標
            var IngredientTarget = GameManager.Instance.players.
                Where(player => player != this).
                OrderByDescending(player => player.IngredientAmount).First();
            // 攻防卡最多目標
            var SpecialCardTarget = GameManager.Instance.players.
                Where(player => player != this).
                OrderByDescending(player => player.SpecialAmount).First();

            var atkCard = item.GetComponent<SpecialCard>();
            switch (atkCard.EffectMethod)
            {
                case "PickPocket":
                case "FoodPoisoning":
                    // 如果目標太窮先不弄他
                    if (RichTarget.Coin < 2) break;
                    atkCard.Effect(this, RichTarget);
                    break;
                case "Thief":
                case "PickyCustomer":
                    // 如果目標食材卡片不足兩張就不弄他
                    if (IngredientTarget.IngredientAmount <= 2) break;
                    atkCard.Effect(this, IngredientTarget);
                    break;
                case "CardPilferer":
                    // 如果目標沒有任何攻防卡就不弄他
                    if (SpecialCardTarget.SpecialAmount == 0) break;
                    atkCard.Effect(this, SpecialCardTarget);
                    break;
                default:
                    atkCard.Effect(this, RichTarget);
                    break;
            }

            yield return new WaitForSeconds(2f);
        }

        MyState = AI_STATE.UseSupportCard;
    }

    protected virtual IEnumerator UseSupportCard()
    {
        if (specialCard.Count < 1)
        {
            MyState = AI_STATE.Cooking;
            yield break;
        }

        // 取出手上之輔助牌並依價值排序
        var query = specialCard.Where(i => i.GetComponent<SpecialCard>().SpecialCardType == SpecialCardType.Support)
            .OrderByDescending(i => i.GetComponent<SpecialCard>().Value);

        if (query.Count() < 1)
        {
            MyState = AI_STATE.Cooking;
            yield break;
        }

        foreach (var special in query)
        {
            yield return new WaitUntil(() => Notify.Instance.isNotifyReady());

            if (special.GetComponent<SpecialCard>().EffectMethod.Equals("HygieneInspection"))
            {
                // 衛生稽查特殊條件
                // 25%機率使用或有其他玩家的錢錢超過5塊錢則使用
                if (Random.Range(1, 100) > 25 ||
                    !(GameManager.Instance.players.Where(p => p != this).Where(p => p.Coin >= 5).Count() > 0))
                {
                    continue;
                }
            }

            special.GetComponent<SpecialCard>().Effect(this);
        }

        MyState = AI_STATE.Cooking;
    }

    protected virtual IEnumerator MakeRecipe()
    {
        yield return new WaitForSeconds(.5f);

        if (hasEffect(PlayerStatus.PAUSE_TAKE_ORDER))
        {
            Notify.Instance.QueueMessage($"由於蟑螂牌的效果影響，{Name}本回合無法做菜。", 1);
            yield return new WaitForSeconds(1f);
            EndTheTurn();
            yield break;
        }

        var query = GetRecipeMakeable();

        while(query.Count() > 0)
        {
            GameObject recipeMakeable = query.OrderByDescending(i => i.GetComponent<RecipeCard>().Value).First();
            RecipeHolder.Instance.PickFullMatchRecipe(recipeMakeable);
            var recipeCard = recipeMakeable.GetComponent<RecipeCard>();
            Debug.Log($"餐點名稱: { recipeCard.Name }, 所需食材數量: { recipeCard.Ingredients.Count()}");
            
            yield return new WaitForSeconds(2f);
            Notify.Instance.QueueMessage($"{Name} 製作了餐點 { recipeCard.Name } ,獲得 { recipeCard.Value } 元", 1);
            Coin += recipeCard.Value;
            
            if (hasEffect(PlayerStatus.TAKE_NEW_CARD_WHEN_ORDER_COMPLETE))
            {
                TakeNewCardSync(1);
                Notify.Instance.QueueMessage($"因產地直送的卡片效果，{Name} 獲得一張食材卡。", 1);
                yield return new WaitUntil(() => Notify.Instance.isNotifyReady());
            }

            var ingredientRequired = recipeCard.Ingredients.ToList();
            int ingredientRequiredCount = recipeCard.Ingredients.Count();

            var cardNeedRemove = new List<GameObject>();
            ingredientCard = ingredientCard.OrderByDescending(item => item.GetComponent<IngredientCard>().m_Ingredient).ToList();
            // 從手牌移除餐點所需的食材
            foreach (var card in ingredientCard)
            {
                var _ingredientCard = card.GetComponent<IngredientCard>();

                if (ingredientRequiredCount > 0)
                {
                    if (_ingredientCard.m_Ingredient == Ingredient.GINSENG)
                    {
                        ingredientRequiredCount -= 2;
                        cardNeedRemove.Add(card);
                        continue;
                    }

                    if (ingredientRequired.Exists(i => i == _ingredientCard.m_Ingredient))
                    {
                        ingredientRequiredCount--;
                        cardNeedRemove.Add(card);
                        continue;
                    }
                }
            }

            CardManager.Instance.DispatchPublicHolder(recipeMakeable.GetComponent<RecipeCard>());
            RecipeHolder.Instance.recipes.Remove(recipeMakeable);

            foreach(GameObject i in cardNeedRemove)
            {
                this.ingredientCard.Remove(i.gameObject);
                CardManager.Instance.DispatchPublicHolder(i.GetComponent<IngredientCard>());
            }

            query = GetRecipeMakeable();
        }

        EndTheTurn();
    }

    protected virtual IEnumerable<GameObject> GetRecipeMakeable()
    {
        // 取得現有食材清單
        HashSet<Ingredient> ingredients = new HashSet<Ingredient>(ingredientCard.Select(item => item.GetComponent<IngredientCard>().m_Ingredient));

        // 取得所有可製作的餐點
        return RecipeHolder.Instance.recipes.Where(item =>
        {
            return item.GetComponent<RecipeCard>().IngredientsFullMatch(ingredients.ToArray());
        });
    }

    protected override void EndTheTurn()
    {
        Debug.Log($"AI EndTheTurn");
        if (CardAmount > 8)
            DropIngredientCard(CardAmount - 8);

        base.EndTheTurn();
        MyState = AI_STATE.Idle;
    }


    public virtual void DropIngredientCard(int dropCount)
    {
        Debug.Log($"AI Need dropCount: {dropCount}");

        ingredientCard = ingredientCard.OrderBy(item => item.GetComponent<IngredientCard>().m_Ingredient).ToList();

        for(int i = 0; i < dropCount; i++)
        {
            CardManager.Instance.DispatchPublicHolder(ingredientCard[0].GetComponent<IngredientCard>());
            ingredientCard.RemoveAt(0);
        }
    }
}
