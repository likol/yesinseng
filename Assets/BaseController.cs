﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BaseController : MonoBehaviour
{
    public delegate void EndTurnHandler();
    public event EndTurnHandler OnEndTurn;

    public Transform ingredientCardHolder;
    public List<GameObject> ingredientCard;
    public List<GameObject> specialCard;
    public GameObject Mask;

    public string Name;
    public int Coin = 0;
    public int CardAmount { get { return IngredientAmount + SpecialAmount; } }
    public int IngredientAmount = 0;
    public int SpecialAmount = 0;
    public bool amILock = true;
    public HashSet<PlayerStatus> playerStatus = new HashSet<PlayerStatus>();
    public Image StatusImage;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        GameManager.Instance.NotifyTurnStart += TurnStart;
        ingredientCard = new List<GameObject>();

        StartCoroutine(TakeNewCard());
        StartCoroutine(TakeNewCard(2, CardType.Speical));
    }

    public virtual IEnumerator TakeNewCard(int amount = 3, CardType cardType = CardType.Ingredient)
    {
        for (int i = 0; i < amount; i++)
        {
            yield return new WaitUntil(() => CardManager.Instance.isReady);
            var cardObj = CardManager.Instance.Take(cardType);
            cardObj.transform.SetParent(ingredientCardHolder);
            if (cardType == CardType.Ingredient)
                ingredientCard.Add(cardObj);
            else if (cardType == CardType.Speical)
                specialCard.Add(cardObj);
        }
    }

    public virtual void TakeNewCardSync(int amount = 3, CardType cardType = CardType.Ingredient)
    {
        for (int i = 0; i < amount; i++)
        {
            var cardObj = CardManager.Instance.Take(cardType);
            cardObj.transform.SetParent(ingredientCardHolder);
            if (cardType == CardType.Ingredient)
                ingredientCard.Add(cardObj);
            else if (cardType == CardType.Speical)
                specialCard.Add(cardObj);
        }
    }

    protected virtual void LateUpdate()
    {
        if (!amILock)
        {
            GetComponent<Image>().color = new Color32(34, 60, 192, 100);
        }
        else
        {
            GetComponent<Image>().color = new Color32(32, 32, 32, 100);
        }
        IngredientAmount = ingredientCard.Count;
        SpecialAmount = specialCard.Count;

        StatusImage.gameObject.SetActive(playerStatus.Contains(PlayerStatus.PAUSE_TAKE_ORDER));
    }

    protected virtual void TurnStart(BaseController controller)
    {
        if (controller != this) return;
        GameManager.Instance.currentPlayer = this;
        amILock = false;
        StartCoroutine(TakeNewCard(3));
        Mask.SetActive(false);
    }
    protected virtual void EndTheTurn() 
    {
        amILock = true;
        Mask.SetActive(true);
        ClearEffect();
        OnEndTurn(); 
    }

    public virtual bool hasEffect(PlayerStatus status) { return playerStatus.Contains(status); }
    public virtual void TakeEffect(PlayerStatus status) { playerStatus.Add(status); }
    public virtual void ClearEffect() { playerStatus.Clear(); }

    public virtual void BuySpecialCard()
    {
        if (Coin < 2)
        {
            Notify.Instance.QueueMessage("金錢不足。", 1);
            return;
        }

        TakeNewCardSync(1, CardType.Speical);
        Notify.Instance.QueueMessage($"{Name} 購買了一張攻防牌。", 1);
        Coin -= 2;
    }
}
