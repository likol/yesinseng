﻿using UnityEngine;
public abstract class BaseCard : MonoBehaviour
{
    public abstract void Initialize();
    public abstract CardType GetCardType();
}
