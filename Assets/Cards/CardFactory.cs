﻿using System;
using System.IO;
using System.Text;
using CsvHelper;
using System.Globalization;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CardFactory : MonoBehaviour
{
    public static GameObject[] Recipes(RectTransform parant)
    {
        GameObject prefab = (GameObject)Resources.Load("Prefabs/RecipeCard");
        var csvText = Resources.Load<TextAsset>("recipes").ToString();
        using (var reader = new StringReader(csvText))
        using (var csvReader = new CsvReader(reader, CultureInfo.CurrentCulture))
        {
            var gameObjects = new List<GameObject>();
            csvReader.Read();
            csvReader.ReadHeader();

            while (csvReader.Read())
            {
                var Name = csvReader.GetField("Name");
                var Value = csvReader.GetField<int>("Value");
                string[] Ingredients = csvReader.GetField("Ingredients").ToString().Split('|');


                GameObject obj = Instantiate(prefab, parant);
                var param = obj.GetComponent<RecipeCard>();
                param.Name = Name;
                param.Value = Value;

                foreach (string s in Ingredients)
                {
                    param.Ingredients.Add((Ingredient)Enum.Parse(typeof(Ingredient), s));
                }

                param.Initialize();

                gameObjects.Add(obj);
            }

            return gameObjects.ToArray();
        }
    }

    public static GameObject[] Ingredients(RectTransform parant)
    {
        GameObject prefab = (GameObject)Resources.Load("Prefabs/IngredientCard");
        var csvText = Resources.Load<TextAsset>("ingredients").ToString();
        using (var reader = new StringReader(csvText))
        using (var csvReader = new CsvReader(reader, CultureInfo.CurrentCulture))
        {
            var gameObjects = new List<GameObject>();
            Ingredient[] RottenType = { Ingredient.ROTTEN_FLOUR, Ingredient.ROTTEN_MEAT, Ingredient.ROTTEN_SEAFOOD, Ingredient.ROTTEN_VEGETABLE, Ingredient.ROTTEN_SOUP };
            csvReader.Read();
            csvReader.ReadHeader();

            while (csvReader.Read())
            {
                var Name = csvReader.GetField("Name");
                var ingredient = csvReader.GetField("Ingredient");
                var amount = csvReader.GetField<int>("Amount");

                for(int i = 0; i < amount; i++)
                {
                    GameObject obj = Instantiate(prefab, parant);
                    var param = obj.GetComponent<IngredientCard>();
                    param.Name = Name;
                    param.m_Ingredient = (Ingredient)Enum.Parse(typeof(Ingredient), ingredient);
                    param.isRotten = RottenType.Contains(param.m_Ingredient);
                    param.Initialize();
                    gameObjects.Add(obj);
                }
            }

            return gameObjects.ToArray();
        }
    }

    public static GameObject[] Special(RectTransform parant)
    {
        GameObject prefab = (GameObject)Resources.Load("Prefabs/SpecialCard");
        var csvText = Resources.Load<TextAsset>("specials").ToString();
        using (var reader = new StringReader(csvText))
        using (var csvReader = new CsvReader(reader, CultureInfo.CurrentCulture))
        {
            var gameObjects = new List<GameObject>();
            csvReader.Read();
            csvReader.ReadHeader();

            while (csvReader.Read())
            {
                var Name = csvReader.GetField("Name");
                var Description = csvReader.GetField("Description");
                var Type = csvReader.GetField("Type");
                var EffectMethod = csvReader.GetField("Effect");
                var amount = csvReader.GetField<int>("Amount");

                for (int i = 0; i < amount; i++)
                {
                    GameObject obj = Instantiate(prefab, parant);
                    var param = obj.GetComponent<SpecialCard>();
                    param.Name = Name;
                    param.Description = Description;
                    param.SpecialCardType = (SpecialCardType)Enum.Parse(typeof(SpecialCardType), Type);
                    param.EffectMethod = EffectMethod;
                    param.Initialize();
                    gameObjects.Add(obj);
                }
            }

            return gameObjects.ToArray();
        }
    }
}
