﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class CardManager : MonoBehaviour
{
    [SerializeField]
    public Stack<GameObject> recipeCards;

    public Stack<GameObject> ingredientCards;

    public Stack<GameObject> specialCards;

    public static CardManager Instance = null;

    public GameObject recipeCardPrefab;

    public RectTransform m_recipeHolder;

    public RectTransform m_template;

    public bool isReady = false;

    private List<GameObject> ingredientPool = new List<GameObject>();
    private List<GameObject> recipePool = new List<GameObject>();
    private List<GameObject> specialPool = new List<GameObject>();

    public int ingredientPoolCount;
    public int speicalPoolCount;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            DestroyImmediate(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        recipeCards = GeneratorNewCardSet(CardType.Recipe);
        ingredientCards = GeneratorNewCardSet(CardType.Ingredient);
        specialCards = GeneratorNewCardSet(CardType.Speical);
        isReady = true;
    }

    private void Update()
    {
        ingredientPoolCount = ingredientPool.Count();
        speicalPoolCount = specialPool.Count();
    }

    Stack<GameObject> GeneratorNewCardSet(CardType type)
    {
        switch(type)
        {
            case CardType.Recipe:
                return new Stack<GameObject>(CardFactory.Recipes(m_template).OrderBy(x => Random.value));
            case CardType.Ingredient:
                return new Stack<GameObject>(CardFactory.Ingredients(m_template).OrderBy(x => Random.value));
            case CardType.Speical:
                return new Stack<GameObject>(CardFactory.Special(m_template).OrderBy(x => Random.value));
            default:
                return null;
        }
    }

    public GameObject Take(CardType type)
    {
        switch (type)
        {
            case CardType.Recipe:
                if (recipeCards.Count() < 1)
                {
                    recipeCards = new Stack<GameObject>(recipePool.OrderBy(x => Random.value));
                    recipePool.Clear();
                }
                return recipeCards.Pop();
            case CardType.Ingredient:
                if (ingredientCards.Count() < 1)
                {
                    ingredientCards = new Stack<GameObject>(ingredientPool.OrderBy(x => Random.value));
                    ingredientPool.Clear();
                }
                return ingredientCards.Pop();
            case CardType.Speical:
                if (specialCards.Count() < 1)
                {
                    specialCards = new Stack<GameObject>(specialPool.OrderBy(x => Random.value));
                    specialPool.Clear();
                }

                return specialCards.Pop();
            default:
                return null;
        }
    }

    public void DispatchPublicHolder(BaseCard card)
    {
        GameObject obj = card.gameObject;
        obj.transform.SetParent(m_template);

        switch(card.GetCardType())
        {
            case CardType.Recipe:
                obj.GetComponent<RecipeCard>().canMake = false;
                obj.transform.Find("Checkmark").gameObject.SetActive(false);
                recipePool.Add(obj);
                break;
            case CardType.Ingredient:
                ingredientPool.Add(obj);
                break;
            case CardType.Speical:
                specialPool.Add(obj);
                break;
        }
    }
}
