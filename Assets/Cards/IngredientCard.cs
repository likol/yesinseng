﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class IngredientCard : BaseCard, IDragHandler, IEndDragHandler, IBeginDragHandler, IPointerClickHandler
{
    public string Name;

    [SerializeField]
    private TextMeshProUGUI name_Text;

    public Ingredient m_Ingredient;
    public CanvasGroup canvasGroup;

    public bool isRotten;
    private Transform previousParent;

    public override void Initialize()
    {
        name_Text.SetText(Name);
        if (isRotten) name_Text.color = new Color32(125, 63, 63, 255);
        GetComponent<Image>().color = IngredientColor.getColor(m_Ingredient);
    }

    public override CardType GetCardType()
    {
        return CardType.Ingredient;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var playerController = GameObject.Find("PlayerZone").GetComponent<PlayerController>();
        if (transform.parent == playerController.ingredientCardHolder)
        {
            playerController.UseIngredientCard(this);
        }
        else
        {
            playerController.UnsetUseIngrdientCard(this);
        }

        Debug.Log($"Current user Ingredients: {playerController.UseIngredients.Count}");
    }

    public void OnDrag(PointerEventData eventData)
    {
        var playerController = GameObject.Find("PlayerZone").GetComponent<PlayerController>();
        if (playerController.amILock) return;
        if (transform.parent == GameObject.Find("DropZone").transform) return;
        transform.SetParent(GameObject.Find("PlayerZone").transform);
        GetComponent<RectTransform>().anchoredPosition += eventData.delta / GameObject.Find("Canvas").GetComponent<Canvas>().scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        var playerController = GameObject.Find("PlayerZone").GetComponent<PlayerController>();
        if (playerController.amILock) return;

        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        transform.SetParent(previousParent);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        var playerController = GameObject.Find("PlayerZone").GetComponent<PlayerController>();
        if (playerController.amILock) return;
        
        previousParent = transform.parent;
        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;
    }
}
