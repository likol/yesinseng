﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RecipeCard : BaseCard
{
    public delegate void OnPickHandler(GameObject recipeCard);
    public event OnPickHandler OnPick;

    [SerializeField]
    public int Value;
    
    [SerializeField]
    public string Name;

    [SerializeField]
    private TextMeshProUGUI value_Text;

    [SerializeField]
    private TextMeshProUGUI name_Text;

    public bool canMake = false;

    void LateUpdate()
    {
        if (canMake)
        {
            GetComponent<Image>().color = new Color32(255, 100, 100, 255);
        } else
        {
            GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }

    }
    public HashSet<Ingredient> Ingredients { get; set; } = new HashSet<Ingredient>();

    public override void Initialize()
    {
        //設定卡片文字
        name_Text.SetText(Name);
        //設定卡片價值
        value_Text.SetText(Value.ToString());

        var colorHolder = transform.Find("Ingredients");

        foreach (Ingredient s in Ingredients)
        {
            colorHolder.Find(s.ToString()).gameObject.SetActive(true);
        }

    }

    public bool IsRecipeIngredient(Ingredient ingredient)
    {
        return Ingredients.Contains(ingredient) || ingredient == Ingredient.GINSENG;
    }

    public bool IngredientsFullMatch(Ingredient[] ingredients)
    {
        bool result = false;

        HashSet<Ingredient> _compareIngredients = new HashSet<Ingredient>(ingredients);

        // 記錄傳入食材是否含有人蔘
        int ginsengCount = Array.FindAll(ingredients, item => item == Ingredient.GINSENG).Count() * 2;

        // 只保留傳入食材與所需食材都有出現的項目
        _compareIngredients.IntersectWith(Ingredients);

        // 當所需食材完全與傳入食材相符
        if (_compareIngredients.SetEquals(Ingredients)) result = true;

        // 如果所需食材長度與人蔘可替代的食材長度相符，則也算為食材全符合
        else if (Ingredients.Count == ginsengCount) result = true;

        else if (Ingredients.Count == ginsengCount + _compareIngredients.Count) result = true;

        canMake = result;

        return canMake;
    }

    public override CardType GetCardType()
    {
        return CardType.Recipe;
    }

    public void OnCardClick()
    {
        Debug.Log($"My Name: {Name}, is full match? {canMake}");
        if (canMake) OnPick(gameObject);
    }
}