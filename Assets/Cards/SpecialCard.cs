﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class SpecialCard : BaseCard, IPointerClickHandler
{
    public string Name;
    public string Description;
    public int Value;

    [SerializeField]
    private TextMeshProUGUI nameText;
    [SerializeField]
    private TextMeshProUGUI typeTopText;
    [SerializeField]
    private TextMeshProUGUI typeBottomText;

    public SpecialCardType SpecialCardType;

    public string EffectMethod;
    public override CardType GetCardType()
    {
        return CardType.Speical;
    }

    public override void Initialize()
    {
        nameText.text = Name;
        string typeString;

        switch(SpecialCardType)
        {
            case SpecialCardType.Attack:
                typeString = "攻";
                break;
            case SpecialCardType.Defence:
                typeString = "防";
                break;
            case SpecialCardType.Support:
                typeString = "輔";
                if (EffectMethod.Equals("HygieneInspection")) typeString = "攻";
                break;
            default:
                typeString = "?";
                break;
        }

        typeTopText.text = typeBottomText.text = typeString;
    }


    public void Effect(BaseController source, BaseController to = null)
    {
        if (to == null)
            Debug.Log($"Source:{source.Name} UseCard: {Name}");
        else
            Debug.Log($"Source:{source.Name} UseCard: {Name} Target: {to.Name}");

        MethodInfo method = GetType().GetMethod(EffectMethod);

        switch (SpecialCardType)
        {
            case SpecialCardType.Support:
                if (EffectMethod.Equals("HygieneInspection"))
                    Notify.Instance.QueueSpecialMessage($"{source.Name} 使用了攻擊卡 >>{this.Name}<<", 2);
                else
                    Notify.Instance.QueueSpecialMessage($"{source.Name} 使用了輔助卡 >>{this.Name}<<", 2);
                break;
            case SpecialCardType.Attack:
                Notify.Instance.QueueSpecialMessage($"{source.Name} 對 {to.Name} 使用了攻擊卡 >>{this.Name}<<", 2);

                if (TargetDefence(source, to))
                {
                    RemoveSpecialCard(source);
                    return;
                }

                break;
            case SpecialCardType.Defence:
                // 夜市之光效果
                if (EffectMethod.Equals("None"))
                {
                    Notify.Instance.QueueSpecialMessage($"{source.Name} 使用夜市之光防禦了 {to.Name} 的攻擊。");
                    RemoveSpecialCard(source);
                    return;
                }
                break;
        }

        var result = (bool)method.Invoke(this, new[] { source, to });

        if (result) RemoveSpecialCard(source);

    }

    public void RemoveSpecialCard(BaseController source)
    {
        //Remove Card from source
        source.specialCard.Remove(this.gameObject);
        CardManager.Instance.DispatchPublicHolder(this);
    }

    public void OnPointerClick(PointerEventData eventData) { SpecialCardFuction.Instance.SetSpecialCard(this); }

    public bool TakeThreeIngredient(BaseController source, BaseController to = null) { source.TakeNewCardSync(); return true; }

    public bool StoreDelivery(BaseController source, BaseController to = null)
    {
        source.TakeNewCardSync(1);
        source.TakeEffect(PlayerStatus.TAKE_NEW_CARD_WHEN_ORDER_COMPLETE);
        return true;
    }

    public bool SpeakEnglish(BaseController source, BaseController to = null) { RecipeHolder.Instance.TakeNewRecipeSync(1); return true; }

    public bool Greedy(BaseController source, BaseController to = null)
    {
        source.TakeNewCardSync(1);
        var card = source.ingredientCard[source.ingredientCard.Count - 1];
        if (card.GetComponent<IngredientCard>().m_Ingredient == Ingredient.VEGETABLE ||
            card.GetComponent<IngredientCard>().m_Ingredient == Ingredient.ROTTEN_VEGETABLE)
        {
            Notify.Instance.QueueSpecialMessage($"{source.Name} 抽到菜食材，再加抽兩張卡片。");
            source.TakeNewCardSync(2);
        }

        return true;
    }

    public bool Thief(BaseController source, BaseController to)
    {
        if (to.ingredientCard.Count < 2) return false;
        
        for (int i = 0; i < 2; i++)
        {
            int randomN = UnityEngine.Random.Range(0, to.ingredientCard.Count - 1);

            var cardObj = to.ingredientCard[randomN];
            source.ingredientCard.Add(cardObj);
            cardObj.transform.SetParent(source.ingredientCardHolder);

            to.ingredientCard.RemoveAt(randomN);
        }

        return true;
    }

    public bool CardPilferer(BaseController source, BaseController to)
    {
        if (to.specialCard.Count < 1) return false;

        int randomN = UnityEngine.Random.Range(0, to.specialCard.Count - 1);
        var cardObj = to.specialCard[randomN];
        source.specialCard.Add(cardObj);
        cardObj.transform.SetParent(source.ingredientCardHolder);
        to.specialCard.RemoveAt(randomN);

        return true;
    }

    public bool Cockroach(BaseController source, BaseController to)
    {
        to.TakeEffect(PlayerStatus.PAUSE_TAKE_ORDER);
        return true;
    }

    public bool HygieneInspection(BaseController source, BaseController to)
    {
        var players = GameManager.Instance.players;

        foreach(var p in players)
        {
            foreach(var item in p.ingredientCard)
            {
                CardManager.Instance.DispatchPublicHolder(item.GetComponent<BaseCard>());
            }
            p.ingredientCard.Clear();
        }

        Notify.Instance.QueueSpecialMessage($"由於 {Name} 的效果，全場玩家失去所有食材卡。");

        return true;
    }

    public bool PickPocket(BaseController source, BaseController to)
    {
        if (to.Coin >= 2)
        {
            to.Coin -= 2;
            source.Coin += 2;
        } else
        {
            int value = to.Coin;
            to.Coin -= value;
            source.Coin += value;
        }
        return true;
    }

    public bool FoodPoisoning(BaseController source, BaseController to)
    {
        if (to.Coin > 0)
        {
            to.Coin--;
            source.Coin++;
        }

        string msg = $"{to.Name}賠償一元給了{source.Name}";

        System.Array ingredients = System.Enum.GetValues(typeof(Ingredient));
        int typeN = UnityEngine.Random.Range(0, ingredients.Length - 1);

        Ingredient drop = (Ingredient)ingredients.GetValue(typeN);
        Ingredient dropSame;
        if (typeN < 5)
            dropSame = (Ingredient)ingredients.GetValue(typeN + 5);
        else if (typeN < 10)
            dropSame = (Ingredient)ingredients.GetValue(typeN - 5);
        else
            dropSame = Ingredient.GINSENG;

        var query = to.ingredientCard.Where(i =>
        {
            return (i.GetComponent<IngredientCard>().m_Ingredient == drop ||
                    i.GetComponent<IngredientCard>().m_Ingredient == dropSame
            );
        }).ToList();

        if (query.Count > 0)
        {
            msg += "且失去了一部分的食材。";

            foreach (var card in query)
            {
                to.ingredientCard.Remove(card);
                CardManager.Instance.DispatchPublicHolder(card.GetComponent<IngredientCard>());
            }
        }

        Notify.Instance.QueueSpecialMessage(msg);

        return true;
    }

    public bool PickyCustomer(BaseController source, BaseController to)
    {
        Notify.Instance.QueueSpecialMessage($"{to.Name} 失去了一半的食材。");
        int amount = Convert.ToInt32(Math.Ceiling((float)to.ingredientCard.Count / 2));

        for (int i = 0; i < amount; i++)
        {
            int randomN = UnityEngine.Random.Range(0, to.ingredientCard.Count - 1);
            var card = to.ingredientCard[randomN];
            CardManager.Instance.DispatchPublicHolder(card.GetComponent<IngredientCard>());
            to.ingredientCard.RemoveAt(randomN);
        }

        return true;
    }

    public bool Police(BaseController source, BaseController to)
    {
        if (to.Coin > 2)
        {
            to.Coin -= 2;
            Notify.Instance.QueueSpecialMessage($"警察對 {to.Name} 進行罰款，查扣了兩元。");
        }
        else
        {
            Notify.Instance.QueueSpecialMessage($"警察沒收了 {to.Name} 的所有食材。");
            foreach (var card in to.ingredientCard)
            {
                CardManager.Instance.DispatchPublicHolder(card.GetComponent<IngredientCard>());
            }
            to.ingredientCard.Clear();
        }
        return true;
    }

    public bool TargetDefence(BaseController source, BaseController to)
    {
        //檢查被攻擊方是否有防禦牌
        var query = to.specialCard.Where(i => i.GetComponent<SpecialCard>().SpecialCardType == SpecialCardType.Defence).ToList();

        if (query.Count == 0) return false;

        //如果被攻擊方有防禦牌則觸發防禦效果

        var def = query.First().GetComponent<SpecialCard>();
        if (def.EffectMethod.Equals("None"))
        {
            def.Effect(to, source);
        }
        else
        {
            def.ReflectAttack(to, source, this);
        }

        return true;
    }

    public bool ThankYouForSavingMe(BaseController def, BaseController atk, SpecialCard originalAtk)
    {
        var players = GameManager.Instance.players.Where(p =>
        {
            return (p.GetComponent<BaseController>() != def && p.GetComponent<BaseController>() != atk);
        }).ToList();

        int randomN = UnityEngine.Random.Range(0, 1);

        var target = players[randomN];
        Notify.Instance.QueueSpecialMessage($"{def.Name} 的 {Name} 將攻擊效果轉移到 {target.Name}身上了。");
        Debug.Log($"Attack: {atk.Name}, Defence: {def.Name}");

        MethodInfo atkMethod = GetType().GetMethod(originalAtk.EffectMethod);
        atkMethod.Invoke(this, new[] { atk, target });
        return true;
    }

    public bool Gangster(BaseController def, BaseController atk, SpecialCard originalAtk)
    {
        Debug.Log($"Attack: {atk.Name}, Defence: {def.Name}");
        Notify.Instance.QueueSpecialMessage($"{def.Name} 的黑社會發生效果，攻擊效果返回至 {atk.Name}。");
        MethodInfo atkMethod = GetType().GetMethod(originalAtk.EffectMethod);
        atkMethod.Invoke(this, new[] { def, atk });
        return true;
    }

    public void ReflectAttack(BaseController def, BaseController atk, SpecialCard originalAtk)
    {
        //反射攻擊
        Debug.Log($"ReflectAttack Card Effect: {EffectMethod}");

        MethodInfo method = GetType().GetMethod(EffectMethod);

        var result = (bool)method.Invoke(this, new object[]{ def, atk, originalAtk });
        RemoveSpecialCard(def);
    }
}
