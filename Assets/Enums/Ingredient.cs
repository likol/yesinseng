﻿public enum Ingredient
{
    ROTTEN_FLOUR,
    ROTTEN_VEGETABLE,
    ROTTEN_MEAT,
    ROTTEN_SEAFOOD,
    ROTTEN_SOUP,
    FLOUR,
    VEGETABLE,
    MEAT,
    SEAFOOD,
    SOUP,
    GINSENG
}