﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class IngredientColor
{
    private static Dictionary<Ingredient, Color32> defaultSet = new Dictionary<Ingredient, Color32>() {
        { Ingredient.FLOUR, new Color32(194, 176, 164, 255) },
        { Ingredient.MEAT, new Color32(225, 111, 77, 255) },
        { Ingredient.SEAFOOD, new Color32(65, 147, 184, 255) },
        { Ingredient.SOUP, new Color32(252, 208, 16, 255) },
        { Ingredient.VEGETABLE, new Color32(176, 185, 39, 255) },
        { Ingredient.GINSENG, new Color32(228, 177, 65, 255) },
        { Ingredient.ROTTEN_FLOUR, new Color32(194, 176, 164, 255) },
        { Ingredient.ROTTEN_MEAT, new Color32(225, 111, 77, 255) },
        { Ingredient.ROTTEN_SEAFOOD, new Color32(65, 147, 184, 255) },
        { Ingredient.ROTTEN_SOUP, new Color32(252, 208, 16, 255) },
        { Ingredient.ROTTEN_VEGETABLE, new Color32(176, 185, 39, 255) },
    };
    
    public static Color32 getColor(Ingredient ingredient)
    {
        return defaultSet[ingredient];
    }
}
