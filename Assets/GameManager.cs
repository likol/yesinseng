﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public delegate void TurnStartHandler(BaseController controller);
    public event TurnStartHandler NotifyTurnStart;

    public static GameManager Instance = null;

    public BaseController[] players = new BaseController[4];
    [Range(1, 65535)]
    public int randomSeed;
    public bool autoRandomSeed = false;
    public int currentPos;
    public BaseController currentPlayer;

    public GameObject GameOverOverlay;
    public TextMeshProUGUI GameOverText;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            DestroyImmediate(gameObject);

        if (autoRandomSeed) randomSeed = UnityEngine.Random.Range(0, 65535);
        UnityEngine.Random.InitState(randomSeed);

        foreach (var player in players)
        {
            player.OnEndTurn += NextTurn;
        }

    }

    void FixedUpdate()
    {
        foreach(var p in players)
        {
            if (p.Coin >= 8)
            {
                Time.timeScale = 0;
                GameOverOverlay.SetActive(true);
                if (p is PlayerController)
                    GameOverText.text = "遊戲結束\n太強了，您贏了 :)";
                else
                    GameOverText.text = "遊戲結束\n很不幸的，您輸了 :(";
            }
        }
    }
    IEnumerator Start()
    {
        Debug.Log($"Curremt game seed: {randomSeed}");
        
        GameOverOverlay.SetActive(false);
        // 設定隨機起始玩家
        currentPos = UnityEngine.Random.Range(0, 3);
        yield return new WaitUntil(() => players.Count() > 0);
        yield return new WaitUntil(() => RecipeHolder.Instance.RecipeIsReady());
        
        NextTurn();
    }

    public void NextTurn()
    {
        Debug.Log($"Game Manager: NextTurn");
        RecipeHolder.Instance.UserPickupRecipe = null;
        NotifyTurnStart(players[currentPos % 4]);
        currentPos++;
    }

    public void NewGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
