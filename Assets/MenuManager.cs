﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject MenuContent;
    public GameObject MenuPanel;
    public GameObject GameRule;

    // Start is called before the first frame update
    void Start()
    {
        GameRule = MenuContent.transform.Find("GameRule").gameObject;
        MenuPanel = MenuContent.transform.Find("MenuPanel").gameObject;

        MenuPanel.transform.Find("seedInfo").GetComponent<TextMeshProUGUI>().text += GameManager.Instance.randomSeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameRule.activeSelf) CloseHelp();
            else TriggerMenu();
        }

    }

    public void TriggerMenu()
    {
        MenuPanel.SetActive(!MenuPanel.activeSelf);
        TriggerPause();
    }
    public void OpenHelp()
    {
        GameRule.SetActive(true);
    }

    public void CloseHelp()
    {
        GameRule.SetActive(false);
    }

    public void TriggerPause()
    {
        Time.timeScale = Mathf.Abs(Time.timeScale - 1);
    }

    public void ExitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.ExitPlaymode();
        #else
            Application.Quit();
        #endif
    }

    public void NewGame()
    {
        TriggerMenu();
        GameManager.Instance.autoRandomSeed = true;
        SceneManager.LoadScene(0);
    }

    public void ResetGame()
    {
        TriggerMenu();
        GameManager.Instance.autoRandomSeed = false;
        SceneManager.LoadScene(0);
    }
}
