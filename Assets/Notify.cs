﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Notify : MonoBehaviour
{
    public TextMeshProUGUI MessageText;
    public TextMeshProUGUI SpecialText;
    public Animator Animator;
    public Queue<KeyValuePair<string, int>> messageQueue;
    public Queue<KeyValuePair<string, int>> specialMessageQueue;
    public static Notify Instance = null;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            DestroyImmediate(gameObject);
    }

    void Start()
    {
        messageQueue = new Queue<KeyValuePair<string, int>>();
        specialMessageQueue = new Queue<KeyValuePair<string, int>>();
    }

    // Update is called once per frame
    void Update()
    {
        if (messageQueue.Count > 0 && Animator.GetBool("Display") == false)
        {
            var temp = messageQueue.Dequeue();
            ShowMessage(temp.Key, temp.Value);
        }
            
        if (specialMessageQueue.Count > 0 && Animator.GetBool("DisplaySpecial") == false)
        {
            var temp = specialMessageQueue.Dequeue();
            ShowSpecialMessage(temp.Key, temp.Value);
        }
    }

    public void QueueSpecialMessage(string _message, int duration = 3)
    {
        specialMessageQueue.Enqueue(new KeyValuePair<string, int>(_message, duration));
    }

    public void QueueMessage(string _message, int duration = 3)
    {
        messageQueue.Enqueue(new KeyValuePair<string, int>(_message, duration));
    }

    public void ShowMessage(string _message, int duration = 3)
    {
        MessageText.text = _message;
        Animator.SetBool("Display", true);

        StartCoroutine(MessageFadeOut(duration));
    }

    public void ShowSpecialMessage(string _message, int duration = 3)
    {
        SpecialText.text = _message;
        Animator.SetBool("DisplaySpecial", true);

        StartCoroutine(SpecialFadeOut(duration));
    }

    private IEnumerator MessageFadeOut(int duration)
    {
        yield return new WaitForSeconds(duration);

        Animator.SetBool("Display", false);
    }

    private IEnumerator SpecialFadeOut(int duration)
    {
        yield return new WaitForSeconds(duration);

        Animator.SetBool("DisplaySpecial", false);
    }

    public bool isNotifyReady()
    {
        return (messageQueue.Count == 0 && specialMessageQueue.Count == 0);
    }
}
