﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : BaseController
{
    public Transform DropZone;
    public List<Ingredient> UseIngredients;

    protected override  void Start()
    {
        Name = "玩家";
        UseIngredients = new List<Ingredient>();
        base.Start();
    }

    public void UseIngredientCard(IngredientCard card)
    {
        if (amILock) return;

        if (DropZone.transform.childCount == 5)
        {
            Notify.Instance.ShowMessage("食材區已滿", 1);
            return;
        }
        var cardIngredient = card.m_Ingredient;
        IEnumerable<GameObject> query = RecipeHolder.Instance.recipes.Where(item => item.GetComponent<RecipeCard>().IsRecipeIngredient(cardIngredient));

        if (query.Count() <= 0)
        {
            Notify.Instance.ShowMessage("沒有餐點需要該食材", 1);
            return;
        }

        card.gameObject.transform.SetParent(DropZone);
        UpdateUsedIngredients();
    }

    public void UnsetUseIngrdientCard(IngredientCard card)
    {
        if (amILock) return;

        card.transform.SetParent(ingredientCardHolder);

        UpdateUsedIngredients();
    }

    public void DestoryRecipeIngredient(Ingredient[] _ingredients)
    {
        var ingredients = new List<Ingredient>(_ingredients);
        var ingredientCount = ingredients.Count();

        var cardInDropZone = ingredientCard.Where(card =>
        {
            return card.gameObject.transform.parent == DropZone.transform;
        }).OrderByDescending(card => card.GetComponent<IngredientCard>().m_Ingredient);


        var cardNeedRemove = new List<GameObject>();

        foreach(var item in cardInDropZone)
        {
            var ingredientCard = item.GetComponent<IngredientCard>();
            UnsetUseIngrdientCard(ingredientCard);
            if (ingredientCount > 0)
            {
                if (ingredientCard.m_Ingredient == Ingredient.GINSENG)
                {
                    ingredientCount -= 2;
                    cardNeedRemove.Add(item);
                    continue;
                }
                
                if (ingredients.Exists(i => i == ingredientCard.m_Ingredient))
                {
                    ingredientCount--;
                    ingredients.Remove(ingredientCard.m_Ingredient);
                    cardNeedRemove.Add(item);
                    continue;
                }
            }

            item.transform.SetParent(ingredientCardHolder);
        }

        cardNeedRemove.ForEach(i => {
            this.ingredientCard.Remove(i.gameObject);
            CardManager.Instance.DispatchPublicHolder(i.GetComponent<IngredientCard>());
        });

        //Debug.Log($"Ingredients Count: {ingredientCount}, DropZoneCount: {cardInDropZone.Count()}");
    }

    public void SendDropZoneCardBack()
    {
        var cardInDropZone = ingredientCard.Where(card =>
        {
            return card.gameObject.transform.parent == DropZone.transform;
        });

        foreach (var item in cardInDropZone)
        {
            UnsetUseIngrdientCard(item.GetComponent<IngredientCard>());
        }
    }

    protected override void EndTheTurn()
    {
        if (amILock) return;

        SendDropZoneCardBack();

        if (CardAmount > 8)
        {
            Notify.Instance.ShowMessage("手牌超過八張，請丟棄一些！", 2);
            return;
        }

        base.EndTheTurn();
    }

    public void UpdateUsedIngredients()
    {
        // 查詢目前在放置區內的卡片
        var query = ingredientCard.Where(item => item.transform.parent == DropZone).ToList();

        if (query.Count == 0) UseIngredients.Clear();
        else
        {
            var relacer = new List<Ingredient>();

            foreach (var card in query)
            {
                relacer.Add(card.GetComponent<IngredientCard>().m_Ingredient);
            }

            UseIngredients = relacer;
        }

        RecipeHolder.Instance.HighlightCards(UseIngredients.ToArray());
    }

}
