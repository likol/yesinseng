﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ProcessRecipe : MonoBehaviour
{
    public PlayerController playerController;
    public RecipeHolder recipeHolder;

    // Update is called once per frame
    void Update()
    {
        if (recipeHolder.UserPickupRecipe != null && !playerController.amILock)
            GetComponent<Button>().interactable = true;
        else
            GetComponent<Button>().interactable = false;
    }

    public void ProcessRecipeAction()
    {
        if (playerController.hasEffect(PlayerStatus.PAUSE_TAKE_ORDER))
        {
            Notify.Instance.QueueMessage($"由於蟑螂牌的效果影響，本回合無法做菜。", 1);
            return;
        }
        RecipeCard recipeCard = recipeHolder.UserPickupRecipe.GetComponent<RecipeCard>();
        Debug.Log($"PickupRecipe: {recipeCard.Name}, Value: {recipeCard.Value}");
        Notify.Instance.QueueMessage($"{GameManager.Instance.currentPlayer.Name} 製作了餐點 {recipeCard.Name}, 獲得 {recipeCard.Value} 元", 3);

        playerController.DestoryRecipeIngredient(recipeCard.Ingredients.ToArray());
        playerController.Coin += recipeCard.Value;

        RecipeHolder.Instance.recipes.Remove(recipeCard.gameObject);
        CardManager.Instance.DispatchPublicHolder(recipeCard);
        recipeHolder.UserPickupRecipe = null;

        if (playerController.hasEffect(PlayerStatus.TAKE_NEW_CARD_WHEN_ORDER_COMPLETE))
        {
            playerController.TakeNewCardSync(1);
            Notify.Instance.QueueMessage($"因產地直送的卡片效果，{GameManager.Instance.currentPlayer.Name} 獲得一張食材卡。", 1);
        }
    }
}
