﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeHolder : MonoBehaviour
{
    public List<GameObject> recipes;

    public static RecipeHolder Instance = null;

    public GameObject UserPickupRecipe;

    public bool Lock = false;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            DestroyImmediate(gameObject);
    }

    void Start()
    {
        recipes = new List<GameObject>();

        StartCoroutine(TakeNewRecipe(3));
    }



    // Update is called once per frame
    void LateUpdate()
    {
        if (recipes.Count < 3 && !Lock)
        {
            StartCoroutine(TakeNewRecipe());
        }
    }

    public IEnumerator TakeNewRecipe(int amount = 1)
    {
        Lock = true;

        for (int i = 0; i < amount; i++)
        {
            yield return new WaitUntil(() => CardManager.Instance.isReady);
            var cardObj = CardManager.Instance.Take(CardType.Recipe);
            cardObj.transform.SetParent(transform);
            cardObj.GetComponent<RecipeCard>().OnPick += PickFullMatchRecipe;
            recipes.Add(cardObj);
        }
        Lock = false;
    }

    public void TakeNewRecipeSync(int amount = 1)
    {
        var cardObj = CardManager.Instance.Take(CardType.Recipe);
        cardObj.transform.SetParent(transform);
        cardObj.GetComponent<RecipeCard>().OnPick += PickFullMatchRecipe;
        recipes.Add(cardObj);
    }

    public void HighlightCards(Ingredient[] list)
    {
        foreach (var obj in recipes)
        {
            bool isCanMake = obj.GetComponent<RecipeCard>().IngredientsFullMatch(list);

            if (UserPickupRecipe == obj)
            {
                if (!isCanMake)
                {
                    UserPickupRecipe.transform.Find("Checkmark").gameObject.SetActive(obj.GetComponent<RecipeCard>().IngredientsFullMatch(list));
                    UserPickupRecipe = null;
                }
            }
                
        }
    }

    public void PickFullMatchRecipe(GameObject cardObj)
    {
        if (UserPickupRecipe != null)
            UserPickupRecipe.transform.Find("Checkmark").gameObject.SetActive(false);

        UserPickupRecipe = cardObj;
        UserPickupRecipe.transform.Find("Checkmark").gameObject.SetActive(true);
    }

    public bool RecipeIsReady()
    {
        return (recipes.Count >= 3);
    }
}
