﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectAttackTarget : MonoBehaviour, IPointerClickHandler
{
    public SpecialCardFuction cardFuction;

    public BaseController player;

    public Color32 baseColor;

    public Color32 selectedColor;

    public void OnPointerClick(PointerEventData eventData)
    {
        cardFuction.targetPlayer = player;
    }

    // Update is called once per frame
    void Update()
    {
        if (cardFuction.targetPlayer == player)
        {
            GetComponent<Image>().color = selectedColor;
        } else
        {
            GetComponent<Image>().color = baseColor;
        }
    }
}
