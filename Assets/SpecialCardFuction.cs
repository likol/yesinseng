﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpecialCardFuction : MonoBehaviour
{
    [SerializeField]
    public SpecialCard specialCard;
    [SerializeField]
    private GameObject panelObject;
    [SerializeField]
    private TextMeshProUGUI cardNameText;
    [SerializeField]
    private TextMeshProUGUI cardDescriptionText;
    [SerializeField]
    private Button UseButton;
    [SerializeField]
    public BaseController targetPlayer;
    [SerializeField]
    public GameObject selectTargetPanel;
    public static SpecialCardFuction Instance;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            DestroyImmediate(gameObject);
    }

    void Start()
    {
        panelObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        panelObject.SetActive((specialCard != null));

        if (UseButton.interactable == false)
        {
            UseButton.interactable = (targetPlayer != null);
        }

        if (specialCard != null)
        {
            UseButton.gameObject.SetActive(specialCard.SpecialCardType != SpecialCardType.Defence);
            selectTargetPanel.SetActive(specialCard.SpecialCardType == SpecialCardType.Attack);
        }
    }

    public void UseCard()
    {
        var from = GameManager.Instance.currentPlayer;
        specialCard.Effect(from, targetPlayer);
        this.Close();
    }

    public void Close()
    {
        this.specialCard = null;
        this.targetPlayer = null;
    }

    public void SetSpecialCard(SpecialCard specialCard)
    {
        this.specialCard = specialCard;
        cardNameText.text = specialCard.Name;
        cardDescriptionText.text = specialCard.Description;
        UseButton.interactable = true;

        if (specialCard.SpecialCardType == SpecialCardType.Attack)
        {
            UseButton.interactable = false;
        }
    }

}
