﻿using TMPro;
using UnityEngine;

public class Status : MonoBehaviour
{
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI MoneyText;
    public TextMeshProUGUI CardAmountText;
    public TextMeshProUGUI IngredientAmountText;
    public TextMeshProUGUI SpecialAmountText;
    public BaseController BaseController;

    // Update is called once per frame
    void Update()
    {
        NameText.text = $"{BaseController.Name}";
        MoneyText.text = $"金錢：{BaseController.Coin}";
        CardAmountText.text = $"手牌數量：{BaseController.CardAmount}";
        IngredientAmountText.text = $"食材卡：{BaseController.IngredientAmount}";
        SpecialAmountText.text = $"攻防卡：{BaseController.SpecialAmount}";
    }
}
