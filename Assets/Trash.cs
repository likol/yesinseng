﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Trash : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public PlayerController playerController;
    public Color32 unActiveColor;
    public Color32 activeColor;
    public void OnDrop(PointerEventData eventData)
    {
        var gameObject = eventData.pointerDrag;
        if (gameObject == null) return;
        if (gameObject.GetComponent<BaseCard>() == null) return;

        var canvasGroup = gameObject.GetComponent<CanvasGroup>();
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        
        transform.parent.GetComponent<PlayerController>().ingredientCard.Remove(gameObject);
        CardManager.Instance.DispatchPublicHolder(gameObject.GetComponent<BaseCard>());
        playerController.UpdateUsedIngredients();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GetComponent<Image>().color = activeColor;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        GetComponent<Image>().color = unActiveColor;
    }


}
